{ pkgs ? import ../nix { }, haskellPackages ? pkgs.haskellPackages
, tag ? "latest" }:
let
  drv = import ./. {
    inherit pkgs;
    inherit haskellPackages;
  };
  staticDrv = pkgs.haskell.lib.justStaticExecutables drv;
in pkgs.dockerTools.buildImage {
  name = "open-aac-server";
  tag = tag;
  config = {
    Cmd = [ "${staticDrv}/bin/open-aac-server" ];
    WorkingDir = "/data";
    Volumes = { "/data" = { }; };
  };
}
