module LibSpec (spec) where

import           Test.Hspec (Spec, describe, it, shouldBe)

spec:: Spec
spec = do
  describe "hello" $ do
    it "says hello" $ do
      "Hello, world!" `shouldBe` "Hello, world!"
