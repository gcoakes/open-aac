self: super: {
  haskellPackages = super.haskellPackages.override {
    overrides = hsSelf: hsSuper: {
      oidc-client = with super.haskell.lib;
        unmarkBroken (dontCheck hsSuper.oidc-client);
    };
  };
}
