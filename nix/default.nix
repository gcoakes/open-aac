args@{ overlays ? [ ], ... }:
let sources = import ./sources.nix;
in import sources.nixpkgs
(args // { overlays = overlays ++ [ (import ./overlay.nix) ]; })
