{ pkgs ? import ./nix { } }:
let
  server = import ./server { inherit pkgs; };
  client = import ./client { inherit pkgs; };
in pkgs.buildEnv {
  name = "open-aac";
  paths = [ server client ];
}
