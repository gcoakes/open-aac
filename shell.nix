{ pkgs ? import ./nix { } }:
let
  drv = import ./. { inherit pkgs; };
  serve-client = pkgs.writeShellScriptBin "serve-client" ''
    cd client && parcel serve src/index.html
  '';
  hash-imgs = with pkgs;
    writeScriptBin "hash-imgs" ''
      #!${python3}/bin/python
      import os
      import json
      from pathlib import Path
      from hashlib import md5
      from shutil import copyfile, move

      os.chdir("client/static")

      with open("tiles.json") as fh:
        tiles = json.load(fh)
      imgs = {}
      for tile in tiles:
        if tile["image"] not in imgs:
          hasher = md5()
          with open(tile["image"], "rb") as fh:
            hasher.update(fh.read())
          src = Path(tile["image"])
          imgs[tile["image"]] = src.parent / (hasher.hexdigest() + src.suffix)
      for original, hashed in imgs.items():
        src = Path(original)
        if src != hashed:
          move(src, hashed, copyfile)
      for tile in tiles:
        tile["image"] = str(imgs[tile["image"]])
      with open("tiles.json", "w") as fh:
        json.dump(tiles, fh)
    '';
  gen-imgs = with pkgs;
    writeShellScriptBin "gen-imgs" ''
      sizes="72x72 96x96 120x120 128x128 144x144 152x152 180x180 192x192 384x384 512x512"
      for size in $sizes; do
        ${imagemagick}/bin/convert client/icons/icon.svg -size "$size" \
          "client/icons/icon-$size.png"
      done
    '';
in pkgs.mkShell {
  name = "open-aac-shell";
  inputsFrom = [ drv ];
  buildInputs = with pkgs; [
    bashInteractive
    cabal-install
    elm2nix
    elmPackages.elm
    elmPackages.elm-format
    gen-imgs
    hash-imgs
    niv
    nodePackages.parcel-bundler
    serve-client
    skopeo
    yarn
  ];
}
