{ pkgs ? import ../nix { } }:
pkgs.mkYarnPackage {
  yarnLock = ./yarn.lock;
  src = pkgs.lib.cleanSourceWith {
    filter = (path: type:
      !builtins.any (v: v == baseNameOf path) [
        "node_modules"
        "dist"
        ".cache"
      ]);
    src = pkgs.lib.cleanSource ./.;
  };

  installPhase = ''
    yarn build --public-url ./ -d "$out"
  '';

  checkPhase = ''
    yarn lint
  '';

  distPhase = ":";
}
