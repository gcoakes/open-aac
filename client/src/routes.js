import Home from "./Page/Home.svelte";
import NotFound from "./Page/NotFound.svelte";

export default [
  {
    path: "/",
    component: Home,
  },
  {
    path: "*",
    component: NotFound,
  },
];
