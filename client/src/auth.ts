import qs from "querystring";
import { onMount, setContext, getContext } from "svelte";
import { Writable, writable } from "svelte/store";
import createAuth0Client from "@auth0/auth0-spa-js";
import { Auth0ClientOptions, Auth0Client } from "@auth0/auth0-spa-js";

const key = {};

type SessionAuth = {
  loading: Writable<Boolean>;
  authenticated: Writable<Boolean>;
  token: Writable<string | null>;
  user: Writable<any | null>;
  error: Writable<string | string[] | null>;
  login: () => Promise<void>;
  logout: () => Promise<void>;
};

export function initSession(config: Auth0ClientOptions): SessionAuth {
  let auth0: Auth0Client = null;
  let session: SessionAuth = {
    loading: writable(true),
    authenticated: writable(false),
    token: writable(null),
    user: writable(null),
    error: writable(null),
    login: async () => {
      auth0.loginWithRedirect({
        redirect_uri: window.location.href,
        prompt: "login",
      });
    },
    logout: async () => {
      auth0.logout({
        returnTo: window.location.href,
      });
    },
  };

  const refreshRate = 10 * 60 * 60 * 1000;

  onMount(async () => {
    let intervalId;
    auth0 = await createAuth0Client(config);
    let updateAuth = async () => {
      let isAuth = await auth0.isAuthenticated();
      session.authenticated.set(isAuth);
      if (isAuth) {
        session.user.set(await auth0.getUser());
        session.token.set(await auth0.getTokenSilently());
        if (!intervalId) {
          intervalId = setInterval(async () => {
            session.token.set(await auth0.getTokenSilently());
          }, refreshRate);
        }
      }
    };
    await updateAuth();
    if (window.location.search.startsWith("?")) {
      let query = qs.parse(window.location.search.slice(1));
      let handled = false;
      if (query.error) {
        session.error.set(query.error_description);
        delete query.error;
        delete query.error_description;
        handled = true;
      } else if (query.code && query.state) {
        await auth0.handleRedirectCallback();
        await updateAuth();
        delete query.code;
        delete query.state;
        handled = true;
      }
      if (handled) {
        let path;
        let queryStr = qs.stringify(query);
        if (queryStr) {
          path = `${window.location.pathname}?${queryStr}`;
        } else {
          path = window.location.pathname;
        }
        window.history.replaceState({}, document.title, path);
      }
    }
    session.loading.set(false);
    return () => {
      intervalId && clearInterval(intervalId);
    };
  });

  setContext(key, session);

  return session;
}

export function getSession() {
  return getContext(key);
}
