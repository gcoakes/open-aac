import { writable } from "svelte/store";

export type Queue<T> = {
  subscribe: (sub: (q: T[]) => void) => void;
  enqueue: (x: T) => number;
  dequeue: () => T;
  clear: () => void;
  set: (q: T[]) => void;
};

export function queue<T>(q: T[] = []) {
  const { subscribe, set, update } = writable(q);

  return {
    subscribe,
    enqueue: (x: T) => {
      var ret: number;
      update((q: T[]) => {
        ret = q.push(x);
        return q;
      });
      return ret;
    },
    dequeue: () => {
      var ret;
      update((q) => {
        ret = q.shift();
        return q;
      });
      return ret;
    },
    clear: () => set([]),
    set,
  };
}
